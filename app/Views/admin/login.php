<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>LOGIN Admin</title>

  <!-- Theme style -->
  <link rel="stylesheet" href="/AdminLTE/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="container">
	<div style="margin: auto" class="card col-sm-5 mt-5">
		<div class="card-header">
			<center><h1>Login Admin</h1></center>
		</div>
		<div class="card-body">
		<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
			<div class="alert alert-warning"><?= $pesan; ?></div>
		<?php } ?>
			<form action="login-proses" method="post">
				<?= csrf_field() ?>
					<label for="username" class="for">Username</label>
						<input
						 type="text"
						 name="username"
						 autofocus
						 autocomplete="off"
						 id="username"
						 class="form-control mb-3"
						 placeholder="admin12345"
						 required
						 minlength="8">
					<label for="password" class="for">Password</label>
						<input
						 type="password"
						 name="password"
						 autofocus
						 autocomplete="off"
						 id="password"
						 class="form-control mb-3"
						 required
						 minlength="8">
				<div class="mb-3 row">
					<button type="submit" class="btn btn-primary col-12">Login</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="/AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/AdminLTE/dist/js/adminlte.min.js"></script>
</body>
</html>