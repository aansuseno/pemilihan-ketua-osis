<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<h3>Tambah Pemilihan - Langkah 1</h3>
<hr>
<form action="/tambah-proses" method="post" class="mt-3">
	<?= csrf_field() ?>
	<div class="mb-3 row">
		<label for="judul" class="form-label col-sm-3">Judul</label>
		<div class="col-sm-9">
			<input
			 type="text"
			 name="judul"
			 autofocus
			 autocomplete="off"
			 required
			 id="judul"
			 class="form-control"
			 title="Isi judul, misal Pemilihan Ketua OSIS <?= date('Y') ?>"
			 placeholder="Pemilihan Ketua OSIS <?= date('Y') ?>">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="mulai" class="form-label col-sm-3">TGL Mulai</label>
		<div class="col-sm-9">
			<input
			 type="text"
			 name="mulai"
			 autocomplete="off"
			 required
			 id="mulai"
			 class="form-control"
			 title="Tanggal dan waktu pemilihan akan dilaksanakan. Isi sesuai format contoh. Tahun-bulan-tgl jam:menit:detik"
			 placeholder="<?= date('Y-m-d H:m:s') ?>"
			 value="<?= date('Y-m-d H:m:s') ?>">
		</div>
	</div>
	<div class="mb-3 row">
		<label for="berakhir" class="form-label col-sm-3">TGL Berakhir</label>
		<div class="col-sm-9">
			<input
			 type="text"
			 name="berakhir"
			 autocomplete="off"
			 required
			 id="berakhir"
			 class="form-control"
			 title="Tanggal dan waktu pemilihan berakhir. Isi sesuai format contoh. Tahun-bulan-tgl jam:menit:detik"
			 placeholder="<?= date('Y-m-d H:m:s') ?>"
			 value="<?= date('Y-m-d H:m:s') ?>">
		</div>
	</div>
	<input type="submit" value="Simpan" class="btn btn-primary">
</form>
<?= $this->endSection() ?>