<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<h4>Detail - <?= $vot['judul'] ?></h4>
<hr>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning mb-2"><?= $pesan; ?></div>
<?php } ?>
<div>
	<button class="btn btn-success" data-toggle="modal" data-target="#popupCalon">Tambah Calon</button>
	<button class="btn btn-dark" data-toggle="modal" data-target="#popupToken">Tambah Token</button>
	<button class="btn btn-warning" data-toggle="modal" data-target="#popupEditWaktu">Edit Waktu</button>
	<button class="btn btn-danger" data-toggle="modal" data-target="#popupHapus"><i class="fas fa-trash"></i></button>
</div>
<hr>
<h5 class="mt-3">Calon :</h5>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around">
	<?php
	$jml = 0;
	foreach ($calon as $c) {
		$jml += $c['jml_pemilih'];
	}
	?>
	<?php foreach ($calon as $c) { ?>
		<div class="card" style="width: 100%; max-width: 700px">
			<div class="card-body">
				<div>
					<?php if ($c['gambar'] != 'null') { ?>
					<img src="/img/<?= $c['gambar'] ?>" style="width: 100%;"></img>
					<?php } ?>        
				</div>
				<button onclick="$('#hapuscalon<?= $c['id'] ?>').show()" class="btn btn-dark"><i class="fas fa-angle-down"></i></button>
			</div>
			<div class="card-footer" id="hapuscalon<?= $c['id'] ?>" style="display: none;">
				<p>Mendapat <?= $c['jml_pemilih'] ?> dari <?= $jml ?> suara. Atau mendapat <?= ($c['jml_pemilih'] != 0) ? $c['jml_pemilih']*100/$jml : '0'?>%.</p>
				<p><?= $c['keterangan'] ?></p>
				<hr>
				<a href="/hapus-calon/<?= $c['id'] ?>" class="btn btn-danger" title="Hapus permanen">
					<i class="fas fa-trash"></i>
				</a>
				<button onclick="$('#hapuscalon<?= $c['id'] ?>').hide()" class="btn btn-dark"><i class="fas fa-times"></i></button>
			</div>
		</div>
	<?php } ?>
</div>
<hr>
<h5 class="mt-3">Token: </h5>
<div style="display: flex; flex-wrap: wrap; justify-content: space-around">
	<?php foreach ($token as $c) { ?>
		<a href="/detail-token/<?= $c->id ?>">
			<div class="card">
				<div class="card-body">
					<?= $c->grup ?>
				</div>
			</div>
		</a>
	<?php } ?>
</div>
<!-- Modal -->
<div class="modal fade" id="popupHapus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">DETAIL CALON !!</h5>
				<button type="button" class="btn" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body" id="detailcalon">
				<p>Anda yakin ingin menghapus secara permanen?</p>
			</div>
			<div class="modal-footer">
				<a href="/hapus-pilihan/<?= $vot['id'] ?>" class="btn btn-danger">Iya</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="popupEditWaktu" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-mb modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">TAMBAH TOKEN !!</h5>
				<button type="button" class="btn" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body" style="max-height: 700px; overflow: scroll;">
				<form action="/edit-waktu" method="post">
					<?= csrf_field() ?>
					<input type="hidden" name="id" value="<?= $vot['id'] ?>">
					<div class="mb-3 row">
						<label for="mulai" class="form-label col-sm-3">TGL Mulai</label>
						<div class="col-sm-9">
							<input
							type="text"
							name="mulai"
							autocomplete="off"
							required
							id="mulai"
							class="form-control"
							title="Tanggal dan waktu pemilihan akan dilaksanakan. Isi sesuai format contoh. Tahun-bulan-tgl jam:menit:detik"
							placeholder="<?= date('Y-m-d H:m:s') ?>"
							value="<?= $vot['mulai'] ?>">
						</div>
					</div>
					<div class="mb-3 row">
						<label for="berakhir" class="form-label col-sm-3">TGL Berakhir</label>
						<div class="col-sm-9">
							<input
							type="text"
							name="berakhir"
							autocomplete="off"
							required
							id="berakhir"
							class="form-control"
							title="Tanggal dan waktu pemilihan berakhir. Isi sesuai format contoh. Tahun-bulan-tgl jam:menit:detik"
							placeholder="<?= date('Y-m-d H:m:s') ?>"
							value="<?= $vot['berakhir'] ?>">
						</div>
					</div>
					<input type="submit" value="Simpan" class="btn btn-primary">
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="popupToken" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-mb modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">TAMBAH TOKEN !!</h5>
				<button type="button" class="btn" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body" style="max-height: 700px; overflow: scroll;">
				<form action="/tambah-token" method="post">
					<?= csrf_field() ?>
					<input type="hidden" name="id_pilihan" value="<?= $vot['id'] ?>">
					<label for="grup">Nama Grup :</label>
					<input type="text" name="grup" class="form-control mb-3" autocomplete="off" required id="grup" placeholder="XII RPL I" title="nama grup supaya mudah mengorganisirnya">
					<label for="jml">Jumlah Token :</label>
					<input type="number" name="jml" class="form-control mb-3" autocomplete="off" min="1" id="jml">
					<button class="btn btn-primary col-12" type="submit">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="popupDetail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">DETAIL CALON !!</h5>
				<button type="button" class="btn" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body" id="detailcalon">
				
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="popupCalon" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">TAMBAH CALON !!</h5>
				<button type="button" class="btn" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body" style="max-height: 700px; overflow: scroll;">
				<form action="/tambah-calon" method="post" enctype="multipart/form-data">
					<?= csrf_field() ?>
					<input type="hidden" name="id_pilihan" value="<?= $vot['id'] ?>">
					<label for="nama_pasangan">Nama Pasangan :</label>
					<input type="text" name="nama_pasangan" class="form-control mb-3" autocomplete="off" required id="nama_pasangan" placeholder="John Doe dan Jane Doe">
					<div class="mb-3">
						<label for="gambar" class="form-label">Gambar : *</label>
						<input type="file" name="file" accept=".png, .jpeg, .gif, .svg, .jpg" class="form-control" id="gambar">
					</div>
					<label for="keterangan">Visi & Misi : *</label>
					<div class="mb-3">
						<textarea name="keterangan" id="keterangan" placeholder="Place some text here"
											style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
					</div>
					<button class="btn btn-primary col-12" type="submit">Simpan</button>
				</form>
				<p><small>* tidak wajib diisi</small></p>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('css') ?>
<link rel="stylesheet" href="/AdminLTE/plugins/summernote/summernote-bs4.css">
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<script src="/AdminLTE/plugins/summernote/summernote-bs4.min.js"></script>
<script>
	$(function () {
		// Summernote
		$('#keterangan').summernote()
	})
</script>
<?= $this->endSection() ?>