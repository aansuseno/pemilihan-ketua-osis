<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<?php $pesan = session()->getFlashdata('pesan'); if (!empty($pesan)) { ?>
	<div class="alert alert-warning mb-2"><?= $pesan; ?></div>
<?php } ?>
<button class="btn btn-success mb-3 mt-3" onclick="tampilKode()">Generate Link</button>
<br>
<code id="kodeHasil" style="display: none"class="mb-3" ></code>
<table class="table table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>KODE</th>
			<th>MEMILIH?</th>
			<th>AKSI</th>
		</tr>
	</thead>
	<tbody>
		<?php $no = 1; foreach ($token as $t) : ?>
		<tr>
			<td><?= $no++ ?></td>
			<td><?= $t['id'].'-'.$t['kode']?></td>
			<td><?= $t['status'] ?></td>
			<td>
				<?php if ($t['status'] == 0) : ?>
				<a href="/hapus-token/<?= $t['id'] ?>" class="btn btn-danger">
					<i class="fas fa-trash"></i>
				</a>
				<?php endif ?>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<?= $this->endSection() ?>

<?= $this->section('js') ?>
<?php $kode = ''; foreach ($token as $t) {
	$kode.=base_url('pilih/'.$t['id'].'-'.$t['kode']).'<br>';
} ?>
<script>
	link = '<?= $kode ?>'
	function tampilKode() {
		$('#kodeHasil').show()
		$('#kodeHasil').html(link)
	}
</script>
<?= $this->endSection() ?>