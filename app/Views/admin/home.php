<?= $this->extend('admin/template') ?>

<?= $this->section('konten') ?>
<a href="/tambah" class="btn btn-primary" title="Tambah pemilihan baru. Misal pemilihan Ketua OSIS">
	<i class="fas fa-plus"></i>
</a>

<div class="wrapper mt-3">
	<?php foreach ($pilihan as $p) { ?>
		<div class="card mb-2">
			<a href="/pilihan/<?= $p['id'] ?>" class="card-body"><?= $p['judul'] ?></a>
		</div>
	<?php } ?>
</div>
<?= $this->endSection() ?>