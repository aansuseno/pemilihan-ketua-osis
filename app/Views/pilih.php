<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Theme style -->
	<link rel="stylesheet" href="/AdminLTE/dist/css/adminlte.min.css">
	<title><?= $pilihan['judul' ] ?></title>
</head>
<body class="wrapper container mt-3 mb-5">
	<h1><?= $pilihan['judul'] ?></h1>
	<hr>
	<div class="alert alert-light">
		<ol>
			<li>Pilih salah satu calon hingga backgroud berganti warna.</li>
			<li>Tidak bisa di kirim apabila belum memilih salah satu calon.</li>
			<li>Untuk menjaga prinsip rahasia, jangan screenshot pilihan.</li>
		</ol>
	</div>
	<form action="/pilih-proses" method="post">
		<?= csrf_field() ?>
		<input type="hidden" name="id_token" value="<?= $token['id'] ?>">
		<input type="text" style="opacity: 0" name="id_calon" required id="id_calon">
		<div style="display: flex; flex-wrap: wrap; justify-content: space-around">
		<?php foreach ($calon as $c) { ?>
			<div class="card" style="width: 100%; max-width: 700px" id="c<?= 
			$c['id'] ?>" onclick="pilih(<?= $c['id'] ?>, 'c<?= 
			$c['id'] ?>')">
				<div class="card-header"><?= $c['nama_pasangan'] ?></div>
				<div class="card-body">
					<?php if($c['gambar'] != 'null') : ?>
						<img src="/img/<?= $c['gambar'] ?>" style="width: 100%;" alt="<?= 'gambar '.$c['nama_pasangan'] ?>">
						<br>
					<?php endif; ?>
					<?= $c['keterangan'] ?>
				</div>
			</div>
		<?php } ?>
		</div>
		<input type="submit" value="KIRIM" class="btn btn-primary col-12">
	</form>
</body>
<script>
	sebelum = ''
	function pilih(id, idc) {
		if (this.sebelum == '') {this.sebelum = idc}
		document.getElementById('id_calon').value = id
		document.getElementById(sebelum).className = "card"
		this.sebelum = idc
		document.getElementById(idc).className += " bg-primary"
	}
</script>
</html>