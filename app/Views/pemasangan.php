<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Pendaftaran Admin</title>

  <!-- Theme style -->
  <link rel="stylesheet" href="/AdminLTE/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="container">
	<div style="margin: auto" class="card col-sm-7 mt-5">
		<div class="card-header">
			<h1>Data diri Admin</h1>
		</div>
		<div class="card-body">
			<form action="/pemasangan-proses" method="post">
				<?= csrf_field() ?>
				<div class="mb-3 row">
					<label for="username" class="form-label col-sm-3">Username</label>
					<div class="col-sm-9">
						<input
						 type="text"
						 name="username"
						 autofocus
						 autocomplete="off"
						 id="username"
						 class="form-control"
						 placeholder="admin12345"
						 required
						 minlength="8">
					</div>
				</div>
				<div class="mb-3 row">
					<label for="password" class="form-label col-sm-3">Password</label>
					<div class="col-sm-9">
						<input
						 type="password"
						 name="password"
						 autofocus
						 autocomplete="off"
						 id="password"
						 class="form-control"
						 required
						 title="untuk keamanan dimohon untuk menggunakan password yg kuat"
						 minlength="8">
					</div>
				</div>
				<div class="mb-3 row">
					<button type="submit" class="btn btn-primary col-12">KIRIM</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="/AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/AdminLTE/dist/js/adminlte.min.js"></script>
</body>
</html>