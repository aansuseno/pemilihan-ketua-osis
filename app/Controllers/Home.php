<?php

namespace App\Controllers;
use App\Models\AdminModel;
use App\Models\Pilihan;
use App\Models\CalonModel;
use App\Models\TokenModel;

class Home extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
		$this->mpilih = new Pilihan();
		$this->mcalon = new CalonModel();
		$this->mtoken = new TokenModel();
	}

    public function index()
    {
		$data = [
			'pilihan' => $this->mpilih->ambil(),
		];
        return view('admin/home', $data);
    }

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/login');
	}

	public function tambahVoting()
	{
		return view('admin/tambahVoting');
	}

	public function tambahVotingProses()
	{
		d($_POST);
		$dataInput = [
			'judul' => $_POST['judul'],
			'mulai' => $_POST['mulai'],
			'berakhir' => $_POST['berakhir'],
			'id_admin' => session('id_admin')
		];
		$this->mpilih->isi($dataInput);
		session()->setFlashdata('pesan', 'Pilihan baru berhasil ditambahkan.');
		return redirect()->to('/pilihan/'.$this->mpilih->query('select * from pilihan order by id desc')->getFirstRow()->id);
	}

	public function detailVoting($id_pilih)
	{
		$data = [
			'vot' => $this->mpilih->ambil(['id' => $id_pilih]),
			'calon' => $this->mcalon->ambilKondisi(['id_pilihan' => $id_pilih]),
			'token' => $this->mtoken->query('select * from token where id_pilihan = '.$id_pilih.' group by grup')->getResult()
		];
		return view('admin/detailVoting', $data);
	}

	public function tambahCalon()
	{
		helper(['form', 'url']);
		$namaGambar = 'null';
		if ($this->request->getFile('file')->getName() != null) {
			$file = $this->validate([
				'file' => [
					'uploaded[file]',
					'max_size[file,10000]',
				]
			]);
			if(!$file) {
				session()->setFlashdata('pesan', 'gagal upload file');
				return redirect()->to('/pilihan/'.$_POST['id_pilihan']);
			}
			$imageFile = $this->request->getFile('file');
			$imageFile->move('img');
			$namaGambar = $imageFile->getName();
		}
		$dataInput = [
			'nama_pasangan' => $_POST['nama_pasangan'],
			'id_pilihan' => $_POST['id_pilihan'],
			'gambar' => $namaGambar,
			'keterangan' => $_POST['keterangan'],
			'jml_pemilih' => 0
		];
		$this->mcalon->isi($dataInput);
		session()->setFlashdata('pesan', 'Calon baru berhasil ditambahkan.');
		return redirect()->to('/pilihan/'.$_POST['id_pilihan']);
	}

	public function hapusCalon($id_calon)
	{
		$he = $this->mcalon->ambil(['id' => $id_calon]);
		$id_pilih = $he['id_pilihan'];
		$namafile = $he['gambar'];
		$this->mcalon->delete(['id' => $id_calon]);
		unlink('img/'.$namafile);
		session()->setFlashdata('pesan', 'Data berhasil dihapus.');
		return redirect()->to('/pilihan/'.$id_pilih);
	}

	public function tambahToken()
	{
		$huruf = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
		$grup = strtoupper($_POST['grup']);
		for ($i=0; $i < $_POST['jml']; $i++) {
			$kode = '';
			for ($j=0; $j < 5; $j++) { 
				$kode .= $huruf[array_rand($huruf)];
			}
			$dataInput = [
				'kode' => $kode,
				'grup' => $grup,
				'id_pilihan' => $_POST['id_pilihan'],
			];
			$this->mtoken->isi($dataInput);
		}
		session()->setFlashdata('pesan', 'Token berhasil dibuat');
		return redirect()->to('/pilihan/'.$_POST['id_pilihan']);
	}

	public function detailToken($id)
	{
		$tok = $this->mtoken->ambil(['id' => $id]);
		$tokens = $this->mtoken->ambilKondisi(['grup' => $tok['grup']]);
		return view('admin/token.php', ['token' => $tokens]);
	}

	public function hapusToken($id)
	{
		$pilihan = $this->mtoken->ambil(['id' => $id]);
		$baru = $this->mtoken->ambil(['id_pilihan' => $pilihan['id_pilihan'], 'grup' => $pilihan['grup']]);
		$this->mtoken->delete(['id' => $id]);
		session()->setFlashdata('pesan', 'Token berhasil dihapus.');
		return redirect()->to('/detail-token/'.$baru['id']);
	}

	public function editWaktu()
	{
		d($_POST);
		$dataEdit = [
			'mulai' => $_POST['mulai'],
			'berakhir' => $_POST['berakhir'],
		];
		$this->mpilih->ubah($dataEdit, ['id' => $_POST['id']]);
		session()->setFlashdata('pesan', 'Waktu pelaksaan pemilihan berhasil diperbarui.');
		return redirect()->to('/pilihan/'.$_POST['id']);
	}

	public function hapusPilihan($id)
	{
		$calon2 = $this->mcalon->ambilKondisi(['id_pilihan' => $id]);
		if (count($calon2) > 0) {
			foreach ($calon2 as $c) {
				unlink('img/'.$c['gambar']);
				$this->mcalon->delete(['id' => $c['id']]);
			}
		}
		$this->mtoken->delete(['id_pilihan' => $id]);
		$this->mpilih->delete(['id' => $id]);
		session()->setFlashdata('pesan', 'Pemilihan berhasil dihapus.');
		return redirect()->to('/home');
	}
}
