<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;

class Login extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
	}

    public function index()
    {
		return view('admin/login');
    }

	public function proses()
	{
		if(count($this->madmin->ambilKondisi(['username'=>$_POST['username'], 'password'=>md5($_POST['password'])])) != 0) {
			session()->login = 'hoke';
			session()->id_admin = $this->madmin->ambil(['username'=>$_POST['username'], 'password'=>md5($_POST['password'])])['id'];
			return redirect()->to('/home');
		}
		session()->setFlashdata('pesan', 'Username dan Password tidak ditemukan.');
		return redirect()->to('/login');
	}
}
