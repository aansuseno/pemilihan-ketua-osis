<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Pilihan;
use App\Models\TokenModel;
use App\Models\CalonModel;

class Pilih extends BaseController
{
	public function __construct() {
		$this->mpilih = new Pilihan();
		$this->mtoken = new TokenModel();
		$this->mcalon = new CalonModel();
	}

    public function index($kode)
    {
		$kodeBaru = explode('-', $kode);
		$token = $this->mtoken->ambil(
			['id' => $kodeBaru[0],
			'kode' => $kodeBaru[1],
			'status' => 0,
		]);
		if($token != null) {
			$pilihan = $this->mpilih->ambil([
				'id' => $token['id_pilihan'],
				'mulai <=' => date('Y-m-d H:m:s'),
				'berakhir >=' => date('Y-m-d H:m:s'),
			]);
			if ($pilihan != null) {
				$calon = $this->mcalon->ambilKondisi(['id_pilihan' => $pilihan['id']]);
				$data = [
					'token' => $token,
					'pilihan' => $pilihan,
					'calon' => $calon,
				];
				return view('pilih', $data);
			} else {
				return view('selesai', ['pesan' => '<h4>Oops. Token tidak bisa digunakan.</h4>']);
			}
		} else {
			return view('selesai', ['pesan' => '<h4>Oops. Token tidak bisa digunakan.</h4>']);
		}
    }

	public function proses()
	{
		$calon = $this->mcalon->ambil(['id' => $_POST['id_calon']]);
		if ($calon != null) {
			$this->mcalon->ubah(['jml_pemilih' => ($calon['jml_pemilih']+1)], ['id' => $calon['id']]);
		}
		$this->mtoken->ubah(['status' => 1], ['id' => $_POST['id_token']]);
		return redirect()->to('selesai');
	}

	public function selesai()
	{
		return view('selesai', ['pesan' => '<h4>Terimakasih sudah menggunakan hak pilih Anda. Token sudah tidak dapat digunakan lagi.</h4>']);
	}
}
