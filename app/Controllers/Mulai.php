<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\AdminModel;

class Mulai extends BaseController
{
	public function __construct() {
		$this->madmin = new AdminModel();
	}

    public function index()
    {
        if (count($this->madmin->ambil()) == 0) {
			return view('pemasangan');
		}
		return redirect()->to('/home');
    }

	public function proses()
	{
		if (count($this->madmin->ambil()) != 0) {
			return redirect()->to('/home');
		}
		$dataInput = [
			'username' => $_POST['username'],
			'password' => md5($_POST['password']),
		];
		$this->madmin->isi($dataInput);
		return redirect()->to('/login');
	}
}
