<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Calon extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 9,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'nama_pasangan'		=> [
                'type'			=> 'VARCHAR',
                'constraint'	=> '255',
            ],
			'gambar'			=> [
                'type'			=> 'VARCHAR',
                'constraint'	=> '255',
            ],
			'keterangan'		=> [
                'type'			=> 'TEXT',
            ],
			'id_pilihan' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'jml_pemilih'	=> [
				'type'		=> 'INT',
				'constraint'=> 9,
				'default' 	=> 0,
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			],
        ]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_pilihan', 'pilihan', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('calon');
    }

    public function down()
    {
        $this->forge->dropTable('calon');
    }
}
