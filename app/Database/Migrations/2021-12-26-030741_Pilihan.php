<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Pilihan extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'				=> [
                'type'			=> 'INT',
                'constraint'	=> 9,
                'unsigned'		=> true,
                'auto_increment'=> true,
            ],
            'judul'       => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'mulai'	=> [
				'type'		=> 'DATETIME'
			],
			'berakhir'	=> [
				'type'		=> 'DATETIME'
			],
			'jml_pemilih' => [
				'type' => 'INT',
				'default' => '0',
				'constraint' => '9'
			],
			'id_admin' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
        ]);
        $this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_admin', 'admin', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('pilihan');
    }

    public function down()
    {
        $this->forge->dropTable('pilihan');
    }
}
