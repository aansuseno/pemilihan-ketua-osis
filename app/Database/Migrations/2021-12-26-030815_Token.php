<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Token extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 9,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
			'kode'			=> [
                'type'			=> 'VARCHAR',
                'constraint'	=> '255',
            ],
			'grup'			=> [
                'type'			=> 'VARCHAR',
                'constraint'	=> '255',
            ],
			'status' => [
				'type' => 'INT',
				'constraint' => 1,
				'default' => 0
			],
			'id_pilihan' => [
				'type'			=> 'INT',
				'constraint'	=> 9,
				'unsigned'		=> true,
			],
			'created_at'	=> [
				'type'		=> 'DATETIME'
			],
			'updated_at'	=> [
				'type'		=> 'DATETIME'
			]
        ]);
		$this->forge->addKey('id', true);
		$this->forge->addForeignKey('id_pilihan', 'pilihan', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('token');
    }

    public function down()
    {
        $this->forge->dropTable('token');
    }
}
