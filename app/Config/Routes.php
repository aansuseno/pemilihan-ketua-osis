<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Mulai::index');
$routes->post('/pemasangan-proses', 'Mulai::proses');
$routes->get('/login', 'Login::index');
$routes->post('/login-proses', 'Login::proses');

$routes->get('/home', 'Home::index', ['filter' => 'login']);
$routes->get('/logout', 'Home::logout', ['filter' => 'login']);
$routes->get('/tambah', 'Home::tambahVoting', ['filter' => 'login']);
$routes->post('/tambah-proses', 'Home::tambahVotingProses', ['filter' => 'login']);
$routes->get('/pilihan/(:any)', 'Home::detailVoting/$1', ['filter' => 'login']);
$routes->post('/tambah-calon', 'Home::tambahCalon', ['filter' => 'login']);
$routes->get('/hapus-calon/(:any)', 'Home::hapusCalon/$1', ['filter' => 'login']);
$routes->post('/tambah-token', 'Home::tambahToken', ['filter' => 'login']);
$routes->get('/detail-token/(:any)', 'Home::detailToken/$1', ['filter' => 'login']);
$routes->get('/hapus-token/(:any)', 'Home::hapusToken/$1', ['filter' => 'login']);
$routes->get('/hapus-pilihan/(:any)', 'Home::hapusPilihan/$1', ['filter' => 'login']);
$routes->post('/edit-waktu', 'Home::editWaktu', ['filter' => 'login']);

$routes->get('/pilih/(:any)', 'Pilih::index/$1');
$routes->get('/selesai', 'Pilih::selesai');
$routes->post('/pilih-proses', 'Pilih::proses');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
